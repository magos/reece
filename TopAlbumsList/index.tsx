import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as Types from "Types";
import "./index.css";
import { albumListComponentMounted } from "../actions";

// MG: Separate model from the component. Model should be usable outside
interface AlbumModel {
  name: string;
  coverImageUrl: string;
}

interface Props {
  albums: ReadonlyArray<AlbumModel>;
  isLoading: boolean;
  onMount: () => void;
}

interface State {
  likedAlbums: Set<number>;
  searchPhrase: string;
}

class ListComponent extends Component<Props, State> {
  // MG: Specify the type of state defined above.
  state = {
    likedAlbums: new Set<number>(),
    searchPhrase: ""
  };

  componentDidMount() {
    this.props.onMount();
  }

  likeAlbum(index: number) {
    // MG: Please avoid direct state mutations. You should use `setState` method.
    // Example: this.setState((prevState) => ({ likedAlbums: prevState.likedAlbums.add(index) }));
    this.state.likedAlbums.add(index);

    // MG: Avoid `forceUpdate()`. Usage of `setState()` will do the trick.
    this.forceUpdate();
  }

  unlikeAlbum(index: number) {
    // MG: Avoid direct state mutations.
    // Example: this.setState((prevState) => ({ likedAlbums: new Set(Array.from(prevState.likedAlbums).filter((i) => i !== index)) }));
    this.state.likedAlbums.delete(index);

    // MG: Get rid of `forceUpdate()`
    this.forceUpdate();
  }

  search(phrase: string) {
    this.setState({ searchPhrase: phrase });
  }

  render() {
    const { albums, isLoading } = this.props;
    return (
      <div className={"Layout"}>
        {isLoading && (
          <div className={"Layout__splash--screen"}>Loading...</div>
        )}
        {!isLoading && albums.length === 0 ? (
          <div className={"Layout__splash--screen"}>Nothing to show</div>
        ) : (
          <Fragment>
            {/* MG: Separate SearchComponent */}
            <input
              className={"Layout__search"}
              value={this.state.searchPhrase}
              onChange={e => this.search(e.target.value)}
              placeholder={"Search album"}
            />
            {albums
              .filter(album => album.name.includes(this.state.searchPhrase))
              .map((album, index) => (
                // MG: Separate ListItemComponent
                <div className={"Card"} key={index}>
                  <img className={"Card__image"} src={album.coverImageUrl} />
                  <h2 className={"Card__text"}>{album.name}</h2>
                  <div className={"Card__like"}>
                    {this.state.likedAlbums.has(index) ? (
                      <i
                        onClick={() => this.unlikeAlbum(index)}
                        className={"Card__like__heart fas fa-heart"}
                      />
                    ) : (
                      <i
                        onClick={() => this.likeAlbum(index)}
                        className={"Card__like__heart far fa-heart"}
                      />
                    )}
                  </div>
                </div>
              ))}
          </Fragment>
        )}
      </div>
    );
  }
}

export default connect(
  (state: Types.RootState) => {
    return {
      albums: state.albums.data.map(
        (item): AlbumModel => ({
          name: item.title,
          coverImageUrl: item.coverImageUrl
        })
      ),
      isLoading: state.albums.metadata.isLoading
    };
  },
  (dispatch: Dispatch<Types.RootAction>) => {
    return {
      onMount: () => dispatch(albumListComponentMounted())
    };
  }
)(ListComponent);
